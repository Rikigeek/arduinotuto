/*
 * Affichage d'un message, caractère par caractère
 */
#define BOUTON 2

// Chaine de caractères, représenté comme un tableau de caractères
char message[] = "BITELLLLL";
// NB: ce compteur est en réalité inutile
int seconde;
// Position dans le message
int mess;

void setup() {
  // Initialisation de la connexion série
  Serial.begin(9600);
  Serial.println("Initialisation");
  // Remise à zéro des compteurs
  seconde = 0;
  mess = 0;
}

void loop() {
  // Serial.println(seconde);
  // Accès au mess-ième caractère de la chaine contenu dans message
  Serial.println(message[mess]);
  delay(1000);
  seconde = seconde + 1;
  mess = mess + 1;

  // strlen pour trouver la longueur de la chaine de caractères contenue dans message
  if (mess>strlen(message))
  {
    // retour au début de la chaine
    mess = 0;
  }
}
