#include <Wire.h> 
#include <LiquidCrystal_I2C.h>

// Les branchements
#define LED 3
#define BOUTON 2
// Les durées d'affichage des symboles morse
// Le point
#define POINT 200
// Le trait
#define TRAIT 600
// Le temps où on éteint la led entre deux symboles
#define BLANC 100
// Le temps où on n'affiche rien, entre 2 caractères
#define ESPACE 250
// Configuration de l'affichage LCD
// Nombre de colonnes (Attention, doit être au moins 3)
#define LCD_COLS 16
// Nombre de lignes
#define LCD_ROWS 2

// Les différents messages en morse à afficher
char couille[] = "-.-./---/..-/../.-../.-../.";
char sos[]     = ".../---/...";
// Pointeur (lien) vers le code morse en course d'affichage
char* code;
// La position courante à l'intérieur du code morse
int position_lecteur;

// Mis à true dès qu'on appuie sur le bouton (déclenché par une interruption)
volatile bool appuie;

// Notre écran LCD. Adresse 0x27 (?), affichage LCD_COLS caractères, LCD_ROWS lignes
LiquidCrystal_I2C lcd(0x27, LCD_COLS, LCD_ROWS);

// Ligne d'affichage des symboles morses affichés
char ligne_morse[LCD_COLS+1]; 


void setup() {
  // Mis à true uniquement quand le bouton est appuyé
  appuie = false;
  // Configuration de la LED en sortie, et du bouton en entrée
  pinMode(LED,    OUTPUT);
  pinMode(BOUTON, INPUT);
  
  // Initialisation de l'écran LCD
  lcd.init();
  lcd.backlight();
  
  // Initialisation du lecteur du code morse
  position_lecteur = 0;
  code = couille;
  // Affichage sur l'écran LCD du message courant
  affiche_LCD("Couille");

  
  init_aff_morse();

  // Activation d'une interruption lors de l'appuie (FALLING) sur le BOUTON. La fonction "stop_it" est appelée
  attachInterrupt(digitalPinToInterrupt(BOUTON), stop_it, FALLING);  

  // informations de débogage sur le moniteur série
  Serial.begin(9600);
  Serial.println("Initialisation");
}

void loop() {
  // On commence par vérifier que le bouton n'a pas été enclenchée. 
  // Cette variable n'est mise à jour que durant la routine d'interruption
  if (appuie) {
    // Interruption par le bouton
    Serial.println("Interruption déclenchée");

    // On change de code à afficher, et on se place au début
    position_lecteur = 0;
    code = sos;
    // Affichage sur l'écran LCD du message courant
    affiche_LCD("SOS");

    // On réinitialise l'indicateur (flag) d'activation du bouton
    appuie = false;
  }

  // Si on arrive à la fin du code morse, et recommence à zéro, avec le code par défaut (couille)
  if (position_lecteur > strlen(code)) {
    position_lecteur = 0;
    code = couille;
    // Affichage sur l'écran LCD du message courant
    affiche_LCD("Couille");
    // Petite pause, car fin du message
    espace();
  }

  // On joue le symbole numéro <position_lecteur> du code morse
  switch (code[position_lecteur]) {
    case '.': aff_morse('.'); point();  break;
    case '-': aff_morse('-'); trait();  break;
    case '/': aff_morse('/'); espace(); break;
  }

  // On affiche quelques informations (position dans le code, et le symbole affiché) sur le moniteur série
  char buf[255];
  sprintf(buf, "[%2d] -> %c", position_lecteur, code[position_lecteur]);
  Serial.println(buf);

  // On passe au symbole suivant dans le code morse en cours de lecture
  position_lecteur++;
}

// Routine d'interruption
void stop_it() {
  // On indique que le bouton a été appuyé
  appuie = true;
}

// initialisation de la ligne des symboles morses avec des espaces.
void init_aff_morse() {
  // mise à blanc (espace) de la ligne_morse qui sera affiché sur la seconde ligne
  for (int i = 0; i < LCD_COLS; i++) {
    ligne_morse[i] = ' ';
  }
  // Ne pas oublier de terminer par le caractère nul pour indiquer la fin de la chaine
  ligne_morse[LCD_COLS] = '\0';
}

// Affiche le symbole morse en cours de lecture
void aff_morse(char c) {
  // On décale tout vers la gauche
  for (int i = 1; i < LCD_COLS; i++) {
    ligne_morse[i-1] = ligne_morse[i];
  }
  // on affiche le dernier symbole reçu à 2 colonnes du bord droit
  ligne_morse[LCD_COLS - 3] = c;
  ligne_morse[LCD_COLS - 2] = '\0';
  // On se place sur la seconde ligne et on affiche la ligne des symboles
  lcd.setCursor(0, 1);
  lcd.print(ligne_morse);
}

// Affichage du message sur la première ligne de l'écran LCD
void affiche_LCD(String string) {
  // Affichage de LCD_COLS espaces sur la première ligne pour l'effacer
  // Construction d'une chaine de la longueur de la ligne
  char buf[LCD_COLS+1];
  for (int i = 0; i < LCD_COLS; i++) {
    buf[i] = ' ';
  }
  buf[LCD_COLS] = '\0';
  // Affichage sur la première ligne
  lcd.setCursor(0,0);
  lcd.print(buf);
  
  // Centrage du texte
  int posX = (LCD_COLS/2) - string.length() / 2;
  lcd.setCursor(posX, 0);
  // et affichage
  lcd.print(string);
}

// Affichage du symbole morse point (symbole .)
void point() {
  digitalWrite(LED, HIGH);
  delay(POINT);
  digitalWrite(LED, LOW);
  delay(BLANC);
}

// Affichage du symbole morse trait (symbole -)
void trait() {
  digitalWrite(LED, HIGH);
  delay(TRAIT);
  digitalWrite(LED, LOW);
  delay(BLANC);
}

// Affichage d'un blanc entre 2 caractères (symbole /)
void espace(){
  digitalWrite(LED, LOW);
  delay(ESPACE); 
  digitalWrite(LED, LOW);
  delay(BLANC);
}

