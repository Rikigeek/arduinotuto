//YWROBOT
//Compatible with the Arduino IDE 1.0
//Library version:1.1
#include <Wire.h> 
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x27,16,2);  // set the LCD address to 0x27 for a 16 chars and 2 line display
int position;
String scroll = "8============D";

void setup()
{
  lcd.init();                      // initialize the lcd 
  lcd.init();
  // Print a message to the LCD.
  lcd.backlight();
  lcd.setCursor(3,0);
  lcd.print("Bouffe moi la ");
  position = 0;
}


void loop()
{
  
  delay(400);
  // On efface la ligne
  lcd.setCursor(0,1);
  lcd.print("                ");
  if (position < 0) {
    lcd.setCursor(0,1);
    lcd.print(scroll.substring(abs(position)));
  }
  else {
    lcd.setCursor(position,1);
    lcd.print(scroll);
  }
  position++;
  if (position > 15) {
    position = -15;
  }
}
