// Les branchements
#define LED 3
#define BOUTON 2
// Les durées d'affichage des symboles morse
// Le point
#define POINT 200
// Le trait
#define TRAIT 600
// Le temps où on éteint la led entre deux symboles
#define BLANC 100
// Le temps où on n'affiche rien, entre 2 caractères
#define ESPACE 250

// Les différents messages en morse à afficher
char couille[] = "-.-./---/..-/../.-../.-../.";
char sos[]     = ".../---/...";
// Pointeur (lien) vers le code morse en course d'affichage
char* code;
// La position courante à l'intérieur du code morse
int position_lecteur;

// Mis à true dès qu'on appuie sur le bouton (déclenché par une interruption)
volatile bool appuie;

void setup() {
  // Mis à true uniquement quand le bouton est appuyé
  appuie = false;
  // Configuration de la LED en sortie, et du bouton en entrée
  pinMode(LED,    OUTPUT);
  pinMode(BOUTON, INPUT);

  // Initialisation du lecteur du code morse
  position_lecteur = 0;
  code = couille;

  // Activation d'une interruption lors de l'appuie (FALLING) sur le BOUTON. La fonction "stop_it" est appelée
  attachInterrupt(digitalPinToInterrupt(BOUTON), stop_it, FALLING);

  // informations de débogage sur le moniteur série
  Serial.begin(9600);
  Serial.println("Initialisation");
}

void stop_it() {
  appuie = true;
}

void loop() {
  // On commence par vérifier que le bouton n'a pas été enclenchée. 
  // Cette variable n'est mise à jour que durant la routine d'interruption
  if (appuie) {
    // Interruption par le bouton
    Serial.println("Interruption déclenchée");

    // On change de code à afficher, et on se place au début
    position_lecteur = 0;
    code = sos;

    // On réinitialise l'indicateur (flag) d'activation du bouton
    appuie = false;
  }

  // Si on arrive à la fin du code morse, et recommence à zéro, avec le code par défaut (couille)
  // et on en profite pour faire une petite pause (car fin du message)
  if (position_lecteur > strlen(code)) {
    position_lecteur = 0;
    code = couille;
    espace();
  }

  // On joue le symbole numéro <position_lecteur> du code morse
  switch (code[position_lecteur]) {
    case '.': point(); break;
    case '-': trait(); break;
    case '/': espace(); break;
  }

  // On affiche quelques informations (position dans le code, et le symbole affiché) sur le moniteur série
  char buf[255];
  sprintf(buf, "[%2d] -> %c", position_lecteur, code[position_lecteur]);
  Serial.println(buf);

  // On passe au symbole suivant dans le code morse en cours de lecture
  position_lecteur++;
}

// Affichage du symbole morse point (symbole .)
void point() {
  digitalWrite(LED, HIGH);
  delay(POINT);
  digitalWrite(LED, LOW);
  delay(BLANC);
}

// Affichage du symbole morse trait (symbole -)
void trait() {
  digitalWrite(LED, HIGH);
  delay(TRAIT);
  digitalWrite(LED, LOW);
  delay(BLANC);
}

// Affichage d'un blanc entre 2 caractères (symbole /)
void espace(){
  digitalWrite(LED, LOW);
  delay(ESPACE); 
}

