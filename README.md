# ArduinoTuto

Sessions de découverte électronique et Arduino.

## Fichier #1 (Première session)
Clignotement d'une led et affichage d'un code morse

## Fichier #2 (Seconde session)
Affichage sur un écran LCD I2C 16x2 caractères

## Fichier #3 (Extra - programmation avancée)
Extension de l'affichage code morse pour gérer l'interruption par le
bouton

## Fichier #4 (Extra - programmation avancée)
Extension du fichier #3 par l'ajout d'un écran LCD affichant:
- le message affiché
- la liste des symboles morses joués

## Fichier #5 (Troisième session)
Gestion des interruptions. Construction d'un chronomètre

## Fichier #6 (Troisième session)
Manipulation de chaines de caractères
Cette troisième session fut l'occasion de voir ces concepts utilisés
dans les fichiers #3 et #4

