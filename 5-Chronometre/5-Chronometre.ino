/* Chronomètre, avec remise à zéro par appuie sur un bouton.
 *  Gestion par interruption
 * 
 */
#define BOUTON 2

// Le compteur du chrono
int seconde;

// L'indicateur d'appuie sur le bouton
volatile bool appuie;

void setup() {
  // Mise en place du moniteur série
  Serial.begin(9600);
  Serial.println("Initialisation");
  // Indicateur d'appuie sur le bouton
  appuie = false;

  // Gestion du bouton
  pinMode(BOUTON, INPUT);
  // Activation d'une interruption sur l'appuie du bouton
  attachInterrupt(digitalPinToInterrupt(BOUTON), stop_it, FALLING);

  // Mise à zéro du chrono
  seconde = 0;
}

/* 
 *  Fonction d'interruption appelée lors de l'appuie sur un bouton
 */
void stop_it() {
  appuie = true;
  
}


void loop() {
  // A chaque seconde, on affiche le nombre de seconde écoulées  
  Serial.println(seconde);
  // Pause de 1 000 ms (soit 1 seconde)
  delay(1000);
  // Incrémenter le compteur du chrono
  seconde = seconde + 1;
  
  // Lors de l'appuie sur le bouton, on remet à zéro le chrono, 
  // et surtout on réinitialise l'indicateur d'appuie sur le bouton
  if (appuie) { 
    appuie = false;
    seconde = 0;
  }

  // remise à zéro automatique au bout de 10 secondes
  if (seconde > 10) {  
    seconde = 0;
  }
  

  
}
