#define LED 1
#define BOUTON 2
#define POINT 200
#define TRAIT 600
#define BLANC 100
#define ESPACE 250




bool appuie;




void setup() {
  pinMode(LED,    OUTPUT);
  pinMode(BOUTON, INPUT);
  appuie = 0;
  attachInterrupt(digitalPinToInterrupt(BOUTON), sos, FALLING);

}

void loop() {
  couille();
  
  delay(100);
}



void couille() {
  digitalWrite(LED,LOW);
  delay (2000);
  trait();
  point();
  trait();
  point();
  espace();
  trait();
  trait();
  trait();
  espace();
  point();
  point();
  trait();
  espace();
  point();
  point();
  espace();
  point();
  trait();
  point();
  point();
  espace();
  point();
  trait();
  point();
  point();
  espace();
  point();
}

void sos() {
  //j'ai voulu ecrire sos avec un delay de deux secondes en amont pour signaler le changement vers sos
  digitalWrite(LED,HIGH);
  delay (2000);
  digitalWrite(LED,LOW);
  delay (500);
  point();
  point();
  point();
  trait();
  trait();
  trait();
  point();
  point();
  point();
  
}

void point() {
  digitalWrite(LED, HIGH);
  delay(POINT);
  digitalWrite(LED, LOW);
  delay(BLANC);
}

void trait() {
  digitalWrite(LED, HIGH);
  delay(TRAIT);
  digitalWrite(LED, LOW);
  delay(BLANC);
}

void espace(){
  digitalWrite(LED, LOW);
  delay(ESPACE); 
}

